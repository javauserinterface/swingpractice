import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingPractice {

    //component가 추가되면 이런식으로 생성된다
    private JButton click;
    private JPanel panelmain;

    //for swing , constuctor is always in the main class
    public SwingPractice() {
        //인터페이스
        click.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "hello");
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
    public static void main(String[] args){
        //class name , it creates the form
        JFrame frame = new JFrame("SwingPractice");
        //set the content of the frame to the jpanel

        frame.setContentPane(new SwingPractice().panelmain);
        //when close , exit the form
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //size manage
        frame.pack();
        //shows on window
        frame.setVisible(true);
    }

}


